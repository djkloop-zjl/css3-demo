function get(el, par) {
    par = par || document;
    return par.querySelector(el);
}